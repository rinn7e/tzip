---
Header has to be copied from the created TZIP.
tzip: TZIP number
title: TZIP title, with an optional name as a prefix (e.g. 'FA1 - Abstract Ledger')
author: Name(s) of author(s), ideally with their username(s) or email address(es)
gratuity: Optional. A Tezos address controlled by an author capable of receiving gratuities from grateful Tezos users
discussions-to: Optional. A url pointing to the official discussion thread
status: <Work In Progress | Draft | Withdrawn | Submitted | Deprecated | Superseded>
type: Defined in TZIP-2
created: Date created on, format yyyy-mm-dd
requires: Optional. Comma-separated TZIP numbers
replaces: Optional. Comma-separated TZIP numbers
superseded-by: Optional. Comma-separated TZIP numbers
---

## Question 1

Answer 1. Answer to the question as descriptive as possible to paint a clear
picture for the reader.


## Question 2

Answer 2

